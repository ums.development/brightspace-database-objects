Create or Replace View lms_course_issue_cancelled
AS
Select CC.code as code
     , CC.TITLE as title
     , CC.INSTITUTION as institution
     , cc.TERM as term
     , CC.CLASS_NUMBER as class_number
     , CC.SESSION_CODE as session_code
     , CC.IS_COMBINED as is_combined
     , 'cancelled' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
Inner Join PS.CS_PS_CLASS_TBL CT
           On CC.INSTITUTION = CT.institution
               And CC.term = CT.strm
               And CC.CLASS_NUMBER = CT.class_nbr
               And CC.IS_COMBINED = 'n'
               And nvl(CC.SESSION_CODE, '<NULL>') = nvl(CT.session_code, '<NULL>')
               And CT.class_stat = 'X'

MINUS

Select CI.CODE
     , CI.TITLE
     , CI.INSTITUTION
     , CI.TERM
     , CI.CLASS_NUMBER
     , CI.SESSION_CODE
     , CI.IS_COMBINED
     , CI.ISSUE
From COURSE_ISSUES CI
Where CI.ISSUE = 'cancelled'
;
