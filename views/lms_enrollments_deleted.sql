CREATE OR REPLACE FORCE VIEW "LMS_ENROLLMENTS_DELETED" AS
Select CC.INSTITUTION
     , CC.TERM
     , CC.CLASS_NUMBER
     , CC.SESSION_CODE
     , CC.IS_COMBINED
     , UC.EMPLID
From ENROLLMENT_COMPLETED EC
Inner JOIN USER_COMPLETED UC
           On EC.USER_LMS_ID = UC.LMS_ID
Inner Join COURSE_COMPLETED CC
           On EC.COURSE_LMS_ID = CC.LMS_ID
Inner Join ENTITY_LOCK EL
           On CC.TERM = EL.TERM
Inner Join TERMS_TO_PROCESS TTP
           On CC.TERM = TTP.TERM
               And CC.INSTITUTION = TTP.INSTITUTION
Where EC.TYPE Not in ('I', 'T')
  And EC.ENROLL_STATUS = 'E'

MINUS

Select institution
     , term
     , class_number
     , session_code
     , is_combined
     , emplid
From (
    Select INSTITUTION
         , STRM term
         , to_char(CLASS_NUMBER) as class_number
         , SESSION_CODE
         , IS_COMBINED
         , EMPLID
    From PS_ENROLL_REGULAR_STUDENT
    Where ENROLL_STATUS = 'E'

    UNION

    Select INSTITUTION
         , strm term
         , CLASS_NUMBER
         , SESSION_CODE
         , IS_COMBINED
         , EMPLID
    From PS_ENROLL_COMBINED_STUDENT
    Where ENROLL_STATUS = 'E'

    UNION

    Select INSTITUTION
         , TERM
         , CLASS_NUMBER
         , SESSION_CODE
         , IS_COMBINED
         , EMPLID
    From LMS_LINKED_ENROLLMENTS
    Where ENROLL_TYPE Not In ('I', 'T')
      And ENROLL_STATUS = 'E'
);
