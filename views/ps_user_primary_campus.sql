Create or Replace Force View "PS_USER_PRIMARY_CAMPUS" as
Select emplid
     , max(INSTITUTION) keep (dense_rank first order by count_dups desc ) as institution
FROM (
    Select emplid
         , institution
         , count(*) as count_dups
    FROM (
        SELECT P.EMPLID
             , SE.INSTITUTION
        From PS.CS_PS_PERSON P
        INNER JOIN PS.CS_PS_STDNT_ENRL SE
                   On P.EMPLID = SE.EMPLID

        UNION ALL

        SELECT P.EMPLID
             , CT.INSTITUTION
        From PS.CS_PS_PERSON P
        INNER JOIN PS.CS_PS_CLASS_INSTR CI
                   On P.EMPLID = CI.EMPLID
        Inner JOIN PS.CS_PS_CLASS_TBL CT
                   On CI.CRSE_ID = CT.CRSE_ID
                       And CI.CRSE_OFFER_NBR = CT.CRSE_OFFER_NBR
                       And CI.STRM = CT.STRM
                       And nvl(CI.SESSION_CODE, '<null>') = nvl(CT.SESSION_CODE, '<null>')
                       And CI.CLASS_SECTION = CT.CLASS_SECTION
    )
    Group By emplid, institution
)
GROUP BY emplid;
