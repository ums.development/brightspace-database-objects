Create Or Replace Force View "LMS_COURSE_PARENTS" As

Select UNIT_COM.LMS_ID as parents
     , UNIT_COM.code as parents_code
     , UNIT_COM.name as parents_name
     , to_char(CO.CLASS_NUMBER) as class_number
     , CO.INSTITUTION
     , CO.TERM
     , CO.IS_COMBINED
     , CO.SESSION_CODE
     , CO.code
From PS_COURSE_OFFERING_O CO
Inner Join college_translation COL
           On CO.institution = COL.institution
               And CO.acad_group = COL.acad_group
               And CO.acad_org = COL.acad_org
               And CO.subject = COL.subject
Inner Join UNIT_TRANSLATION UT
           On CO.institution = UT.INSTITUTION
               And CO.acad_group = nvl(UT.acad_group, CO.acad_group)
               And CO.acad_org = nvl(UT.acad_org, CO.acad_org)
               And CO.subject = nvl(UT.subject, CO.subject)
Inner Join COLLEGE_COMPLETED COLL_COM
           On COL.college = COLL_COM.CODE
Inner Join UNIT_COMPLETED UNIT_COM
           On COL.COLLEGE || '_' || UT.UNIT = UNIT_COM.CODE
               And UNIT_COM.PARENT = COLL_COM.LMS_ID

Union

Select CLASSES.parents
     , CLASSES.parents_code
     , CLASSES.parents_name
     , CC.SCTN_COMBINED_ID as class_number
     , CC.INSTITUTION
     , CC.strm as term
     , 'y' as is_combined
     , CC.SESSION_CODE
     , CC.CODE
From PS_COMBINED_COURSES_O CC
INNER JOIN
(
    Select INSTITUTION
         , STRM
         , SESSION_CODE
         , SCTN_COMBINED_ID
         , listagg(LMS_ID, '|') WITHIN GROUP ( ORDER BY LMS_ID) as parents
         , listagg(code, '|') WITHIN GROUP ( ORDER BY LMS_ID) as parents_code
         , listagg(name, '|') WITHIN GROUP ( ORDER BY LMS_ID) as parents_name
    From (
        Select CC.institution
             , CC.strm
             , CC.session_code
             , CC.SCTN_COMBINED_ID
             , UC.lms_id
             , UC.code
             , UC.name
        From PS_COMBINED_CLASSES_O CC
        Inner Join college_translation COL
                   On CC.institution = COL.institution
                       And CC.acad_group = COL.acad_group
                       And CC.acad_org = COL.acad_org
                       And CC.subject = COL.subject
        Inner Join UNIT_TRANSLATION UT
                   On CC.institution = UT.INSTITUTION
                       And CC.acad_group = nvl(UT.acad_group, CC.acad_group)
                       And CC.acad_org = nvl(UT.acad_org, CC.acad_org)
                       And CC.subject = nvl(UT.subject, CC.subject)
        Inner Join UNIT_COMPLETED UC
                   On COL.COLLEGE || '_' || UT.UNIT = UC.CODE
                       And UC.PARENT = (
                           Select LMS_ID
                           From COLLEGE_COMPLETED inCC
                           Where inCC.CODE = COL.COLLEGE
                       )
        GROUP BY CC.institution
               , CC.strm
               , CC.session_code
               , CC.SCTN_COMBINED_ID
               , UC.LMS_ID
               , UC.CODE
               , UC.name
    )
    group by INSTITUTION, STRM, SESSION_CODE, SCTN_COMBINED_ID
) CLASSES
On CC.INSTITUTION = CLASSES.INSTITUTION
    And CC.STRM = CLASSES.STRM
    And nvl(CC.SESSION_CODE, '<null>') = nvl(CLASSES.SESSION_CODE, '<null>')
    And CC.SCTN_COMBINED_ID = CLASSES.SCTN_COMBINED_ID;
