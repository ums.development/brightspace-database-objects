Create or Replace View admin_role_overrides as
Select RO.USER_LMS_ID
     , RO.ROLE_IND
     , UC.USERNAME
     , UC.EMPLID
     , UC.FIRST_NAME
     , UC.LAST_NAME
From ROLE_OVERRIDE RO
         Inner Join USER_COMPLETED UC
                    On RO.USER_LMS_ID = UC.LMS_ID
Order By UC.username;
