CREATE OR REPLACE FORCE VIEW "LMS_USERS_UPDATES" AS
Select COM.LMS_ID
     , U.username
     , U.FIRST_NAME
     , U.MIDDLE_NAME
     , U.LAST_NAME
     , u.EMPLID
     , u.EMAIL
From ps_user U
INNER JOIN USER_COMPLETED COM
           On U.EMPLID = COM.EMPLID
               And
              (
                      nvl(U.FIRST_NAME, '<null>') <> nvl(COM.FIRST_NAME, '<null>')
                      Or nvl(U.LAST_NAME, '<null>') <> nvl(COM.LAST_NAME, '<null>')
                      Or nvl(U.MIDDLE_NAME, '<null>') <> nvl(COM.MIDDLE_NAME, '<null>')
                      Or nvl(U.USERNAME, '<null>') <> nvl(COM.USERNAME, '<null>')
                      Or nvl(U.EMAIL, '<null>') <> nvl(COM.EMAIL, '<null>')
                  )
GROUP BY COM.LMS_ID, U.emplid, u.USERNAME, u.EMAIL, U.LAST_NAME, U.FIRST_NAME, U.MIDDLE_NAME;
