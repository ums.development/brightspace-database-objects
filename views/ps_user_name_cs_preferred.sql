Create or Replace Force View "PS_USER_NAME_CS_PREFERRED" as
SELECT PRF.emplid
     , PRF.first_name
     , PRF.last_name
     , PRF.middle_name
From PS.CS_PS_NAMES PRF
Where PRF.name_type = 'PRF'
  And PRF.eff_status = 'A'
  And nvl(PRF.effdt, trunc(sysdate)) =
      (
          Select nvl(max(effdt), trunc(sysdate))
          From PS.CS_PS_NAMES inN
          Where inN.emplid = PRF.emplid
            And inN.effdt <= trunc(sysdate)
            And inN.name_type = 'PRF'
      );
