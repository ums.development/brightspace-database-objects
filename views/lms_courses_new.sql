CREATE OR REPLACE FORCE VIEW "LMS_COURSES_NEW" AS
Select CODE
     , TITLE
     , PARENTS
     , MASTER
     , SEMESTER
     , CLASS_NUMBER
     , INSTITUTION
     , TERM
     , IS_COMBINED
     , SESSION_CODE
     , START_DT
     , END_DT
     , IS_ACTIVE
From (
    Select C.code
         , C.title
         , C.parents
         , C.master
         , C.semester
         , C.CLASS_NUMBER
         , C.INSTITUTION
         , C.TERM
         , C.IS_COMBINED
         , C.SESSION_CODE
         , C.START_DT
         , C.END_DT
         , C.IS_ACTIVE
    From lms_courses C
    Where not exists(
            Select 1
            From COURSE_COMPLETED CC
            Where CC.IS_COMBINED = C.IS_COMBINED
              And CC.INSTITUTION = C.INSTITUTION
              And CC.TERM = C.TERM
              And CC.CLASS_NUMBER = C.CLASS_NUMBER
              And nvl(CC.SESSION_CODE, '<null>') = nvl(C.SESSION_CODE, '<null>')
        )
)
Where rownum < 41;
