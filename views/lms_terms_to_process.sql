CREATE OR REPLACE FORCE VIEW "LMS_TERMS_TO_PROCESS" AS
Select TTP.INSTITUTION
     , TTP.TERM
     , UC.SHORT_NAME          as          campus_short_name
     , UC.name                as          campus_name
     , nvl(SC.NAME, TTP.TERM) as          term_name
     , UC.LMS_ID                          CAMPUS_LMS_ID
     , nvl(SC.LMS_ID, '-1')               TERM_LMS_ID
     , UC.TEMPLATE_LMS_ID_FALL                 CAMPUS_TEMPLATE_LMS_ID_FALL
     , UC.TEMPLATE_LMS_ID_SPRING                 CAMPUS_TEMPLATE_LMS_ID_SPRING
     , UC.TEMPLATE_LMS_ID_SUMMER               CAMPUS_TEMPLATE_LMS_ID_SUMMER
     , TTP.INSTITUTION || '-' || TTP.TERM pk
From TERMS_TO_PROCESS TTP
     Inner Join UNIVERSITY_COMPLETED UC
                On TTP.INSTITUTION = substr(UC.CODE, 0, 5)
     Left Outer Join SEMESTER_COMPLETED SC
                     On TTP.TERM = SC.CODE
Order By TTP.INSTITUTION, TTP.TERM;
