Create or Replace Force View "PS_USER" as
Select Distinct EMPLIDS.EMPLID
              , nvl(HR.USERNAME, CS.USERNAME) username
              , nvl(HR.EMAIL, CS.email) email
              , nvl(HR.LAST_NAME, CS.LAST_NAME) last_name
              , nvl(HR.FIRST_NAME, CS.FIRST_NAME) first_name
              , nvl(HR.MIDDLE_NAME, CS.MIDDLE_NAME) middle_name
From (
    SELECT HR.EMPLID
    From ps_user_hr HR

    Union

    SELECT CS.EMPLID
    From ps_user_cs CS
) EMPLIDS
LEFT OUTER JOIN PS_USER_HR HR
                On EMPLIDS.EMPLID = HR.EMPLID
LEFT OUTER JOIN PS_USER_CS CS
                On EMPLIDS.EMPLID = CS.EMPLID
