CREATE OR REPLACE FORCE VIEW lms_roles_updates AS
Select USER_LMS_ID
    , role_ind
From (
    Select R.ROLE_IND
        , R.USER_LMS_ID
    From LMS_ROLES R
    Inner Join ROLE_COMPLETED RC
        On R.USER_LMS_ID = RC.USER_LMS_ID
            And R.ROLE_IND <> RC.ROLE_IND
)
Where ROWNUM < 1001;
