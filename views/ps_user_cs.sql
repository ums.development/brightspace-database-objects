Create or Replace Force View "PS_USER_CS" as
SELECT SE.EMPLID
     , substr(E.email_addr, 1, instr(E.email_addr, '@', 1, 1) - 1) as username
     , E.email_addr as email
     , min(UN.LAST_NAME) as last_name
     , min(UN.FIRST_NAME) as first_name
     , min(UN.MIDDLE_NAME) as middle_name
From (
    Select emplid
    From PS.CS_PS_STDNT_ENRL
    Where strm > to_char(to_number(to_char(sysdate, 'YY')) - 1) || '00'

    Union

    Select emplid
    From PS.CS_PS_ADM_APPL_PROG
    Where PROG_STATUS in ('PM', 'WT', 'AC', 'AD', 'AP')
      And admit_term > to_char(to_number(to_char(sysdate, 'YY')) - 1) || '00'

    Union

    Select emplid
    From PS.CS_PS_STDNT_CAR_TERM SCT
    Where strm > to_char(to_number(to_char(sysdate, 'YY')) - 1) || '00'
) SE
Inner Join PS.CS_PS_EMAIL_ADDRESSES E
           On SE.emplid = E.emplid
               And E.e_addr_type = 'BUSN'
               And lower(email_addr) like '%@maine.edu'
Inner Join ps_user_name_cs UN
           On SE.EMPLID = UN.EMPLID
Group By SE.EMPLID, E.email_addr;
