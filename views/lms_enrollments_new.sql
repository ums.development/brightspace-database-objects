CREATE OR REPLACE FORCE VIEW "LMS_ENROLLMENTS_NEW" AS
Select E.user_lms_id
     , E.course_lms_id
     , E.type
     , E.enroll_status
From LMS_ENROLLMENTS E
Where (E.USER_LMS_ID, E.COURSE_LMS_ID) Not IN
      (
          Select EC.USER_LMS_ID, Ec.COURSE_LMS_ID
          From ENROLLMENT_COMPLETED EC
      );
