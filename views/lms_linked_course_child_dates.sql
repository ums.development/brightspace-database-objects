CREATE OR REPLACE FORCE VIEW "LMS_LINKED_COURSE_CHILD_DATES" AS
Select LCP.INSTITUTION
     , LCP.TERM
     , LCP.CLASS_NUMBER
     , LCP.SESSION_CODE
     , LCP.IS_COMBINED
     , LCP.CODE as parent_code
     , min(CO.start_dt) as start_dt
     , max(CO.end_dt) as end_dt
From LINKED_COURSE_PARENT LCP
Inner Join ENTITY_LOCK EL
           On LCP.TERM = EL.TERM
Inner Join TERMS_TO_PROCESS TTP
           On LCP.TERM = TTP.TERM
               And LCP.INSTITUTION = TTP.INSTITUTION
INNER JOIN LINKED_COURSE_CHILD LCC
           On LCP.CODE = LCC.PARENT_CODE
Inner Join PS_COURSE_OFFERING CO
           On LCC.INSTITUTION = CO.institution
               And LCC.term = CO.TERM
               And LCC.CLASS_NUMBER = CO.CLASS_NUMBER
               And nvl(LCC.SESSION_CODE, '<null>') = nvl(CO.SESSION_CODE, '<null>')
               And LCC.IS_COMBINED = 'n'
Group By LCP.INSTITUTION, LCP.TERM, LCP.CLASS_NUMBER, LCP.SESSION_CODE, LCP.IS_COMBINED, LCP.code

Union

Select LCP.INSTITUTION
     , LCP.TERM
     , LCP.CLASS_NUMBER
     , LCP.SESSION_CODE
     , LCP.IS_COMBINED
     , LCP.code as parent_code
     , min(CC.start_dt) as start_dt
     , max(CC.end_dt) as end_dt
From LINKED_COURSE_PARENT LCP
Inner Join ENTITY_LOCK EL
           On LCP.TERM = EL.TERM
Inner Join TERMS_TO_PROCESS TTP
           On LCP.TERM = TTP.TERM
               And LCP.INSTITUTION = TTP.INSTITUTION
INNER JOIN LINKED_COURSE_CHILD LCC
           On LCP.CODE = LCC.PARENT_CODE
Inner Join PS_COMBINED_CLASSES CC
           On LCC.INSTITUTION = CC.institution
               And LCC.term = CC.strm
               And LCC.CLASS_NUMBER = CC.SCTN_COMBINED_ID
               And nvl(LCC.SESSION_CODE, '<null>') = nvl(CC.SESSION_CODE, '<null>')
               And LCC.IS_COMBINED = 'y'
Group By LCP.INSTITUTION, LCP.TERM, LCP.CLASS_NUMBER, LCP.SESSION_CODE, LCP.IS_COMBINED, LCP.code;
