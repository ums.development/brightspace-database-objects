Create or Replace Force View "PS_USER_NAME_HR" as
SELECT HR.emplid
     , nvl(PRF.FIRST_NAME, PRI.first_name) first_name
     , nvl(PRF.last_name, PRI.last_name) last_name
     , nvl(PRF.middle_name, PRI.MIDDLE_NAME) middle_name
From (
    Select emplid
    From PS_USER_NAME_HR_PRIMARY
    UNION
    Select emplid
    From PS_USER_NAME_HR_PREFERRED
) HR
Left Outer Join PS_USER_NAME_HR_PRIMARY PRI
                On HR.EMPLID = PRI.EMPLID
LEFT OUTER JOIN PS_USER_NAME_HR_PREFERRED PRF
                On HR.EMPLID = PRF.EMPLID;
