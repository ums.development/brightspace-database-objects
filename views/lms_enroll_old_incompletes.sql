CREATE OR REPLACE FORCE VIEW "LMS_ENROLL_OLD_INCOMPLETES" AS
Select EBC.user_lms_id
     , EBC.course_lms_id
     , EBC.type
     , EBC.enroll_status
From ENROLLMENT_COMPLETED EC
Inner Join COURSE_COMPLETED CC
           On EC.ENROLL_STATUS = 'E'
               And EC.TYPE = 'X'
               And (CC.INSTITUTION, CC.TERM) not in (
                   Select INSTITUTION, TERM
                   From TERMS_TO_PROCESS
               )
               And EC.COURSE_LMS_ID = CC.LMS_ID
Inner Join LMS_ENROLLMENTS_BY_CLASS EBC
           On EC.COURSE_LMS_ID = EBC.COURSE_LMS_ID
               And EC.USER_LMS_ID = EBC.USER_LMS_ID
               And (
                      EC.ENROLL_STATUS <> EBC.ENROLL_STATUS
                      Or
                      EC.TYPE <> EBC.TYPE
                  )

Union All

Select EBC.user_lms_id
     , EBC.course_lms_id
     , EBC.type
     , EBC.enroll_status
From ENROLLMENT_COMPLETED EC
Inner Join COURSE_COMPLETED CC
           On EC.ENROLL_STATUS = 'E'
               And EC.TYPE = 'S'
               And (CC.INSTITUTION, CC.TERM) not in (
                   Select INSTITUTION, TERM
                   From TERMS_TO_PROCESS
               )
               And EC.COURSE_LMS_ID = CC.LMS_ID
Inner Join LMS_ENROLLMENTS_BY_CLASS EBC
           On EC.COURSE_LMS_ID = EBC.COURSE_LMS_ID
               And EC.USER_LMS_ID = EBC.USER_LMS_ID
               And EBC.ENROLL_STATUS = 'E'
               And EBC.TYPE = 'X'
;
