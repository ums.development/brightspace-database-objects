CREATE OR REPLACE FORCE VIEW "LMS_COURSES" AS
Select code
     , (
    Case
        When length(title) > 128 Then
                trim(substr(substr(title, 1, instr(title, '(', -1) - 2), 1, 110)) || '... ' ||
                substr(title, instr(title, '(', -1))
        Else
            title
        End
    ) as title
     , parents
     , master
     , semester
     , C.class_number
     , C.institution
     , C.term
     , C.is_combined
     , C.session_code
     , Case
           When CS.SET_START_DATE = 1 Then
               Case
                   When LCCD.start_dt < C.start_dt Then
                       to_char(LCCD.START_DT + CS.START_DATE_DAYS_OFFSET, 'YYYY-MM-DD')
                   Else
                       to_char(C.START_DT + CS.START_DATE_DAYS_OFFSET, 'YYYY-MM-DD')
                   End
           Else
               null
    End as start_dt
     , Case
           When CS.SET_END_DATE = 1 Then
               Case
                   When LCCD.end_dt > C.end_dt Then
                       to_char(LCCD.END_DT + CS.END_DATE_DAYS_OFFSET, 'YYYY-MM-DD')
                   Else
                       to_char(C.END_DT + CS.END_DATE_DAYS_OFFSET, 'YYYY-MM-DD')
                   End
           Else
               null
    End as end_dt
     , CS.IS_ACTIVE
From (
    Select code,
           title,
           parents,
           master,
           semester,
           to_char(class_number) as class_number,
           institution,
           term,
           is_combined,
           session_code,
           START_DT,
           END_DT
    From LMS_COURSES_REGULAR

    Union

    Select CODE,
           TITLE,
           PARENTS,
           MASTER,
           SEMESTER,
           CLASS_NUMBER,
           INSTITUTION,
           TERM,
           IS_COMBINED,
           SESSION_CODE,
           START_DT,
           END_DT
    From LMS_COURSES_COMBINED
) C
Inner Join COURSE_SETTINGS CS
           On C.INSTITUTION = CS.INSTITUTION
Left Outer Join LMS_LINKED_COURSE_CHILD_DATES LCCD
                On LCCD.PARENT_CODE = C.code;
