CREATE OR REPLACE FORCE VIEW "LMS_COURSES_REGULAR" AS
Select CO.code
     , CO.title
     , UNIT_COM.LMS_ID as parents
     , DECODE(substr(CO.TERM, 3, 1),
              '1',
              nvl(UNIT_COM.TEMPLATE_LMS_ID_FALL, nvl(COLL_COM.TEMPLATE_LMS_ID_FALL, UNIV_COM.TEMPLATE_LMS_ID_FALL)),
              '2', nvl(UNIT_COM.TEMPLATE_LMS_ID_SPRING,
                       nvl(COLL_COM.TEMPLATE_LMS_ID_SPRING, UNIV_COM.TEMPLATE_LMS_ID_SPRING)),
              '3', nvl(UNIT_COM.TEMPLATE_LMS_ID_SUMMER,
                       nvl(COLL_COM.TEMPLATE_LMS_ID_SUMMER, UNIV_COM.TEMPLATE_LMS_ID_SUMMER)),
              null) as master
     , SC.LMS_ID as semester
     , CO.CLASS_NUMBER
     , CO.INSTITUTION
     , CO.TERM
     , CO.IS_COMBINED
     , CO.SESSION_CODE
     , CO.START_DT
     , CO.END_DT
From PS_COURSE_OFFERING CO
Inner Join college_translation COL
           On CO.institution = COL.institution
               And CO.acad_group = COL.acad_group
               And CO.acad_org = COL.acad_org
               And CO.subject = COL.subject
Inner Join UNIT_TRANSLATION UT
           On CO.institution = UT.INSTITUTION
               And CO.acad_group = nvl(UT.acad_group, CO.acad_group)
               And CO.acad_org = nvl(UT.acad_org, CO.acad_org)
               And CO.subject = nvl(UT.subject, CO.subject)
Inner Join COLLEGE_COMPLETED COLL_COM
           On COL.college = COLL_COM.CODE
Inner Join UNIT_COMPLETED UNIT_COM
           On COL.COLLEGE || '_' || UT.UNIT = UNIT_COM.CODE
               And UNIT_COM.PARENT = COLL_COM.LMS_ID
Inner Join UNIVERSITY_COMPLETED UNIV_COM
           On CO.institution = substr(UNIV_COM.CODE, 1, 5)
Inner Join SEMESTER_COMPLETED SC
           On CO.TERM = SC.CODE;
