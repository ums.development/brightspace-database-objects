Create Or Replace Force View "PS_COMBINED_COURSES_O" As
Select SCT.strm || '.' || SCT.institution || '-C.' || SCT.SCTN_COMBINED_ID || '.' || SCT.session_code as code
     , CCT.TITLE
     , SCT.institution
     , SCT.strm
     , SCT.session_code
     , SCT.sctn_combined_id
     , CSC.start_dt
     , CSC.end_dt
From PS.CS_PS_SCTN_CMBND_TBL SCT
Inner Join PS_COMBINED_COURSES_TITLE_O CCT
           On SCT.institution = CCT.institution
               And SCT.strm = CCT.strm
               And nvl(SCT.session_code, '<null>') = nvl(CCT.session_code, '<null>')
               And SCT.sctn_combined_Id = CCT.sctn_combined_id
               And SCT.strm > 1100
Inner Join (
    Select CC.INSTITUTION
         , CC.STRM
         , CC.SCTN_COMBINED_ID
         , CC.SESSION_CODE
         , min(CC.START_DT) as start_dt
         , max(CC.END_DT) as end_dt
    From PS_COMBINED_CLASSES_O CC
    Group By CC.INSTITUTION, CC.STRM, CC.SCTN_COMBINED_ID, CC.SESSION_CODE
) CSC
           On SCT.institution = CSC.institution
               And SCT.strm = CSC.strm
               And nvl(SCT.session_code, '<null>') = nvl(CSC.session_code, '<null>')
               And SCT.sctn_combined_Id = CSC.sctn_combined_id;
