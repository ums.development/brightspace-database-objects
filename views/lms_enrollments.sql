CREATE OR REPLACE FORCE VIEW "LMS_ENROLLMENTS" AS
Select UC.LMS_ID as user_lms_id
     , CC.LMS_ID as course_lms_id
     , ENROLL.ENROLL_TYPE as type
     , ENROLL.enroll_status
From (
    Select INSTITUTION
         , term
         , CLASS_NUMBER
         , SESSION_CODE
         , IS_COMBINED
         , EMPLID
         -- This is a not so clever hack to limit the enrollments to a single record per class/person.
         -- This will determine the role/type by getting the enrolled row for that user/class.
         -- If there is more than 1, then decode will pick the highest ranked.
         -- If there are no enrolled rows (a delete), then learner (S) is assumed.
         -- This is needed to fix BSI-196.
         , substr(
                (listagg(
                        Case
                            When ENROLL_STATUS = 'E' then ENROLL_TYPE
                            Else ''
                            End
                    , '') within group ( order by decode(ENROLL_TYPE, 'I', 1, 'N', 2, 'T', 3, 'X', 4, 'S', 5))) || 'S'
        , 1
        , 1
        ) as ENROLL_TYPE
         , Case
               When Sum(Case When ENROLL_STATUS = 'E' Then 1 Else 0 End) > 0 Then
                   'E'
               Else
                   'D'
        End ENROLL_STATUS
    From (
        Select E.INSTITUTION
             , E.strm term
             , E.CLASS_NUMBER
             , E.SESSION_CODE
             , E.IS_COMBINED
             , E.EMPLID
             , E.ENROLL_TYPE
             , E.ENROLL_STATUS
        From PS_ENROLLMENTS E

        UNION All

        Select LE.INSTITUTION
             , LE.TERM
             , LE.CLASS_NUMBER
             , LE.SESSION_CODE
             , LE.IS_COMBINED
             , LE.EMPLID
             , LE.ENROLL_TYPE
             , LE.ENROLL_STATUS
        From LMS_LINKED_ENROLLMENTS LE

        UNION All

        Select DE.INSTITUTION
             , DE.TERM
             , DE.CLASS_NUMBER
             , DE.SESSION_CODE
             , DE.IS_COMBINED
             , DE.EMPLID
             , 'S' enroll_type
             , 'D' enroll_status
        From lms_enrollments_deleted DE
    )
    GROUP BY INSTITUTION, term, CLASS_NUMBER, SESSION_CODE, IS_COMBINED, EMPLID
) ENROLL
INNER JOIN COURSE_COMPLETED CC
           On ENROLL.CLASS_NUMBER = CC.CLASS_NUMBER
               And ENROLL.INSTITUTION = CC.INSTITUTION
               And nvl(ENROLL.SESSION_CODE, '<null>') = nvl(CC.SESSION_CODE, '<null>')
               And ENROLL.term = CC.TERM
               And ENROLL.is_combined = CC.IS_COMBINED
Inner Join USER_COMPLETED UC
           On ENROLL.EMPLID = UC.EMPLID;
