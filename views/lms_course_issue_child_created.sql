Create or Replace View lms_course_issue_child_created
AS
Select CC.code
     , CC.TITLE
     , CC.INSTITUTION
     , CC.TERM
     , CC.CLASS_NUMBER
     , CC.SESSION_CODE
     , CC.IS_COMBINED
     , 'linked_child_created' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.TERM = TTL.TERM
               And CC.INSTITUTION = TTL.INSTITUTION
Inner Join LINKED_COURSE_CHILD LCC
           On CC.code = LCC.CODE

Union All

Select CC.code
     , CC.TITLE
     , CC.INSTITUTION
     , CC.TERM
     , CC.CLASS_NUMBER
     , CC.SESSION_CODE
     , CC.IS_COMBINED
     , 'csc_child_created' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.TERM = TTL.TERM
               And CC.INSTITUTION = TTL.INSTITUTION
Inner Join PS_COMBINED_CLASSES PCC
           On CC.INSTITUTION = PCC.INSTITUTION
               And CC.TERM = PCC.STRM
               And CC.CLASS_NUMBER = PCC.CLASS_NBR
               And nvl(CC.SESSION_CODE, '<NULL>') = nvl(PCC.SESSION_CODE, '<NULL>')
               And CC.IS_COMBINED = 'n'
               And CC.TERM > '2110'
               And PCC.STRM > '2110'

MINUS

Select CI.CODE
     , CI.TITLE
     , CI.INSTITUTION
     , CI.TERM
     , CI.CLASS_NUMBER
     , CI.SESSION_CODE
     , CI.IS_COMBINED
     , CI.ISSUE
From COURSE_ISSUES CI
Where CI.ISSUE = 'linked_child_created'
   Or CI.ISSUE = 'csc_child_created'
;