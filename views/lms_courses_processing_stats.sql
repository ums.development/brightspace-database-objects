CREATE OR REPLACE FORCE VIEW "LMS_COURSES_PROCESSING_STATS" AS
Select count(*) queued_courses
     , 40 process_quantity
From (
    Select C.code
         , C.title
         , C.parents
         , C.master
         , C.semester
         , C.CLASS_NUMBER
         , C.INSTITUTION
         , C.TERM
         , C.IS_COMBINED
         , C.SESSION_CODE
    From lms_courses C
    Where IS_COMBINED = 'n'
      And not exists(
            Select 1
            From COURSE_COMPLETED CC
            Where CC.IS_COMBINED = 'n'
              And CC.INSTITUTION = C.INSTITUTION
              And CC.TERM = C.TERM
              And CC.CLASS_NUMBER = C.CLASS_NUMBER
        )

    UNION

    Select C.code
         , C.title
         , C.parents
         , C.master
         , C.semester
         , C.CLASS_NUMBER
         , C.INSTITUTION
         , C.TERM
         , C.IS_COMBINED
         , C.SESSION_CODE
    From lms_courses C
    Where IS_COMBINED <> 'n'
      And not exists(
            Select 1
            From COURSE_COMPLETED CC
            Where CC.IS_COMBINED <> 'n'
              And CC.INSTITUTION = C.INSTITUTION
              And CC.TERM = C.TERM
              And CC.CLASS_NUMBER = C.CLASS_NUMBER
              And CC.SESSION_CODE = C.SESSION_CODE
        )
);
