Create or Replace View lms_course_issue_deleted
AS
Select CC.code as code
     , CC.TITLE as title
     , CC.INSTITUTION as institution
     , cc.TERM as term
     , CC.CLASS_NUMBER as class_number
     , CC.SESSION_CODE as session_code
     , CC.IS_COMBINED as is_combined
     , 'deleted' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
               And CC.IS_COMBINED = 'n'
Left Outer Join PS.CS_PS_CLASS_TBL CT
                On CC.INSTITUTION = CT.institution
                    And CC.term = CT.strm
                    And CC.CLASS_NUMBER = CT.class_nbr
                    And nvl(CC.SESSION_CODE, '<NULL>') = nvl(CT.session_code, '<NULL>')
Where CT.class_nbr is null

Union All

Select CC.code as code
     , CC.TITLE as title
     , CC.INSTITUTION as institution
     , cc.TERM as term
     , CC.CLASS_NUMBER as class_number
     , CC.SESSION_CODE as session_code
     , CC.IS_COMBINED as is_combined
     , 'deleted' as issue
From COURSE_COMPLETED CC
Inner Join TERMS_TO_PROCESS TTL
           On CC.INSTITUTION = TTL.INSTITUTION
               And CC.TERM = TTL.TERM
               And CC.IS_COMBINED = 'y'
Left Outer Join PS.CS_PS_SCTN_CMBND_TBL SCT
                On CC.INSTITUTION = SCT.institution
                    And CC.term = SCT.strm
                    And CC.CLASS_NUMBER = SCT.sctn_combined_id
                    And nvl(CC.SESSION_CODE, '<NULL>') = nvl(SCT.session_code, '<NULL>')
Where SCT.sctn_combined_id is null

MINUS

Select CI.CODE
     , CI.TITLE
     , CI.INSTITUTION
     , CI.TERM
     , CI.CLASS_NUMBER
     , CI.SESSION_CODE
     , CI.IS_COMBINED
     , CI.ISSUE
From COURSE_ISSUES CI
Where CI.ISSUE = 'deleted'
;
