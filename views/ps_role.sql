Create or Replace View PS_ROLE as
Select emplid
     , min(role_ind) keep ( dense_rank first order by ranking ) as role_ind
From
(
    --Incarcerated Students
    Select SG.emplid
         , 'P' role_ind
         , 1 ranking
    From PS.CS_PS_STDNT_GRPS SG
    Where SG.institution = 'UMS01'
      And SG.stdnt_group = '1DOC'

    Union

    --Non-FERPA Compliant Instructors
    Select emplid
        , 'N' role_ind
        , 2 ranking
    From
    (
        Select CI.emplid
        From PS.CS_PS_CLASS_INSTR CI
        Where CI.strm > to_char(sysdate, 'YY') || '00'
          And CI.INSTR_ROLE In ('PI', 'SI')
        Minus
        Select roleuser as emplid
        From PS.CS_PSROLEUSER
        Where rolename in ('S_SS_FACULTY', 'S_SS_FACULTY_ADJUNCT')
    )

    Union

    --Instructors
    Select CI.emplid
           , 'I' role_ind
           , 4 ranking
      From PS.CS_PS_CLASS_INSTR CI
      Where CI.strm > to_char(sysdate, 'YY') || '00'
        And CI.instr_role in ('PI', 'SI')

      Union

      --Teacher's Assistants
      Select CI.emplid
           , 'T' role_ind
           , 5 ranking
      From PS.CS_PS_CLASS_INSTR CI
      Where CI.strm > to_char(sysdate, 'YY') || '00'
        And CI.instr_role = 'TA'

      Union

      --Employees, Persons of Interest
      Select emplid
        , 'S' role_ind
        , 6 ranking
      From PS_USER_HR

      Union

      --Students enrolled in classes
      Select SE.emplid
           , 'S' role_ind
           , 6 ranking
      From PS.CS_PS_STDNT_ENRL SE
      Where SE.strm > to_char(sysdate, 'YY') || '00'
        And SE.stdnt_enrl_status = 'E'
        And SE.enrl_status_reason <> 'WDRW'

      Union

      --Students admitted to a program
      Select emplid
           , 'S' role_ind
           , 6 ranking
      From PS.CS_PS_ADM_APPL_PROG
      Where admit_term > to_char(sysdate, 'YY') || '00'
        And PROG_STATUS in ('PM', 'WT', 'AC', 'AD', 'AP')
        And admit_term > to_char(to_number(to_char(sysdate, 'YY')) - 1) || '00'

      Union

      --Students that are quick admitted
      Select emplid
           , 'S' role_ind
           , 6 ranking
      From PS.CS_PS_STDNT_CAR_TERM SCT
      Where strm > to_char(to_number(to_char(sysdate, 'YY')) - 1) || '00'
)
Group By emplid
;
