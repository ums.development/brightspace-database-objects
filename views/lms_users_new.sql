CREATE OR REPLACE FORCE VIEW "LMS_USERS_NEW" AS
Select U.username
     , U.FIRST_NAME
     , U.MIDDLE_NAME
     , U.LAST_NAME
     , u.EMPLID
     , u.EMAIL
From ps_user U
Where U.EMPLID not in
      (
          Select EMPLID
          From USER_COMPLETED
      )
And rownum < 251;
