# Brightspace Database Objects

Database objects used by the Brightspace apps ([ETL](https://gitlab.com/ums.development/brightspace-etl) and Administrator Utility).

## Note to the Public

This is written and maintained for the use of the [University of Maine System](https://www.maine.edu/). Feature requests will likely not be acted on. Merge requests will be reviewed, but there is no guarantee that they will be accepted.
