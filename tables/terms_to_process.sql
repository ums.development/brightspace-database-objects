create table terms_to_process
(
    institution  varchar2(10)   not null,
    term         varchar2(10)   not null
);

create index TERMS_TO_PROCESS_INST_IDX
    on TERMS_TO_PROCESS (INSTITUTION);

create index TERMS_TO_PROCESS_TERM_IDX
    on TERMS_TO_PROCESS (term);