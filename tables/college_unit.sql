create table college_unit
(
    id          NUMBER,
    college varchar2(100) not null,
    unit varchar2(100) not null
);

CREATE SEQUENCE college_unit_seq START WITH 1;

CREATE OR REPLACE TRIGGER college_unit_trig
    BEFORE INSERT ON college_unit
    FOR EACH ROW

BEGIN
    SELECT college_unit_seq.NEXTVAL
    INTO   :new.id
    FROM   dual;
END;



