create table course_completed
(
    code         varchar2(100)  not null,
    title        varchar2(1000) not null,
    parents      varchar2(1000) not null,
    master       varchar2(100)  not null,
    semester     varchar2(100)  not null,
    class_number varchar2(50)   not null,
    institution  varchar2(10)   not null,
    term         varchar2(10)   not null,
    is_combined  varchar2(10),
    session_code varchar2(100),
    added_on     timestamp      not null,
    modified_on  timestamp      not null,
    LMS_ID       varchar2(100)  not null,
    start_dt     date   default null,
    end_dt       date   default null,
    is_active    number default 0
);

create index course_completed_class_nbr_idx
    on course_completed (class_number);

create index course_completed_inst_idx
    on course_completed (institution);

create index course_completed_term_idx
    on course_completed (term);

create index course_completed_is_comb_idx
    on course_completed (is_combined);

create index course_completed_sess_code_idx
    on course_completed (session_code);
