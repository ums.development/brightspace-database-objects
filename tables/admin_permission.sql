create table admin_permission
(
    id int generated as identity,
    category    varchar2(1000),
    route       varchar2(200)       not null,
    action      varchar(10)         not null,
    is_active   number(1) default 1 not null,
    description varchar2(1000)      not null,
    name        varchar2(100)       not null,
    CONSTRAINT admin_permission_pk PRIMARY KEY (id)
);
