create table ADMIN_USER
(
    id int generated as identity,
    username varchar2(100) unique not null,
    show_admin_help_info number(1) default 0 not null,
    is_active number(1) default 1 not null,
    CONSTRAINT admin_user_pk PRIMARY KEY (id)
);
