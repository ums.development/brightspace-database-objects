Create Table ENTITY_LOCK
(
    entity    varchar2(50)  not null,
    is_locked int default 0 not null,
    term      varchar2(10)
);
