create table sandbox_completed
(
    emplid      varchar2(20) not null,
    lms_id      varchar2(50) not null,
    added_on    timestamp,
    modified_on timestamp
);

create unique index sandbox_completed_emplid_index
    on sandbox_completed (emplid);

create unique index sandbox_completed_lmsid_index
    on sandbox_completed (lms_id);
