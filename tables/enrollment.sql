create table enrollment
(
    pk           int generated as identity,
    class_number varchar2(10) not null,
    term         varchar2(4)  not null,
    campus       varchar2(5)  not null,
    is_combined  varchar(10)  not null,
    session_code varchar2(10),
    emplid       varchar2(10) not null,
    type         varchar2(10) not null
);
