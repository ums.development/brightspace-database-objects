create table admin_page
(
    id int generated as identity,
    route_name  varchar2(100)    not null,
    description varchar2(250)    not null,
    is_active   number(1) default 1 not null,
    CONSTRAINT admin_page_pk PRIMARY KEY (id)
);
