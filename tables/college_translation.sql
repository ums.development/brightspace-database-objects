create table college_translation
(
    id          NUMBER,
    institution varchar2(10) not null,
    acad_group varchar2(100) not null,
    subject varchar2(100) not null,
    acad_org varchar2(100),
    college varchar2(100) not null
);

/*create sequence for auto-increment column*/
CREATE SEQUENCE college_translation_seq START WITH 1;

/*create trigger for auto-increment column*/
CREATE OR REPLACE TRIGGER college_translation_trig
    BEFORE INSERT ON college_translation
    FOR EACH ROW

BEGIN
    SELECT college_translation_seq.NEXTVAL
    INTO   :new.id
    FROM   dual;
END;
