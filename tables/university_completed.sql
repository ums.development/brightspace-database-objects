create table university_completed
(
    code            varchar2(100) not null,
    name            varchar2(100) not null,
    short_name      varchar2(50) not null,
    lms_id          varchar2(50)  not null,
    added_on        timestamp,
    modified_on     timestamp,
    template_lms_id_fall varchar2(50),
    template_lms_id_spring varchar2(50),
    template_lms_id_summer varchar2(50)
);

create unique index university_completed_code
    on university_completed (code);

create unique index university_completed_name
    on university_completed (name);

create unique index university_completed_lmsid
    on university_completed (lms_id);
