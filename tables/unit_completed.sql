create table unit_completed
(
    code            varchar2(100) not null,
    name            varchar2(100) not null,
    parent          varchar2(100) not null,
    lms_id          varchar2(50)  not null,
    added_on        timestamp,
    modified_on     timestamp,
    template_lms_id_fall varchar2(50),
    template_lms_id_spring varchar2(50),
    template_lms_id_summer varchar2(50)
);

create unique index unit_completed_code
    on unit_completed (code);

create unique index unit_completed_lmsid
    on unit_completed (lms_id);
