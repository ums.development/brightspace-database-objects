CREATE OR REPLACE PROCEDURE add_new_course_org_as_unknown
As
Begin
    --Special case for UMPI I-DIST courses that don't have a college translation defined
    Insert Into COLLEGE_TRANSLATION
    (INSTITUTION, ACAD_GROUP, SUBJECT, ACAD_ORG, COLLEGE)
    Select CT.institution, CT.acad_group, CT.subject, CT.acad_org, 'IDIST' College
    From PS.CS_PS_CLASS_TBL CT
    Inner Join TERMS_TO_PROCESS TTP
               On CT.institution = TTP.INSTITUTION
                   And CT.strm = TTP.TERM
    Where CT.institution = 'UMS07'
      And CT.acad_org = 'I-DIST'
    Group By CT.institution, CT.acad_group, CT.acad_org, CT.subject

    MINUS

    Select INSTITUTION, ACAD_GROUP, SUBJECT, ACAD_ORG, 'IDIST'
    From COLLEGE_TRANSLATION
        commit;

    -- All other courses without a college translation defined
    Insert Into COLLEGE_TRANSLATION
        (INSTITUTION, ACAD_GROUP, SUBJECT, ACAD_ORG, COLLEGE)
    Select CT.institution, CT.acad_group, CT.subject, CT.acad_org, CT.institution || '_UNKNOWN' College
    From PS.CS_PS_CLASS_TBL CT
    Inner Join TERMS_TO_PROCESS TTP
               On CT.institution = TTP.INSTITUTION
                   And CT.strm = TTP.TERM
    Group By CT.institution, CT.acad_group, CT.acad_org, CT.subject

    MINUS

    Select INSTITUTION, ACAD_GROUP, SUBJECT, ACAD_ORG, institution || '_UNKNOWN'
    From COLLEGE_TRANSLATION commit;
End;